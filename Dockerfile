FROM python:3.7-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        git=1:2.20.1-2+deb10u1 \
        mercurial=4.8.2-1+deb10u1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /usr/bin

WORKDIR /usr/bin

RUN pip install -r requirements.txt

COPY pipe /usr/bin/
COPY LICENSE.txt pipe.yml README.md /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/pipe.py"]
